package com.example.task.database;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "user_table")
public class User {
    /// Commit From Shahir

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String staffNo;
    private String password;
    private String name;
    private String dob;
    private String mobile;
    private String designation;
    private int shop;
    private String department;

    public User(String staffNo, String password, String name, String dob, String mobile, String designation, int shop, String department) {
        this.staffNo = staffNo;
        this.password = password;
        this.name = name;
        this.dob = dob;
        this.mobile = mobile;
        this.designation = designation;
        this.shop = shop;
        this.department = department;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStaffNo() {
        return staffNo;
    }

    public void setStaffNo(String staffNo) {
        this.staffNo = staffNo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public int getShop() {
        return shop;
    }

    public void setShop(int shop) {
        this.shop = shop;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }
}
